package ru.chetverikov.vkcup2020g

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.google.gson.Gson
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiConfig
import com.vk.api.sdk.VKDefaultValidationHandler
import com.vk.api.sdk.VKTokenExpiredHandler
import ru.chetverikov.vkcup2020g.ui.LauncherActivity
import ru.chetverikov.vkcup2020g.util.DefaultActivityLifecycleCallbacks

class App : Application() {

	override fun onCreate() {
		super.onCreate()
		VK.setConfig(
			VKApiConfig(
				context = this,
				validationHandler = VKDefaultValidationHandler(this),
				lang = "ru"
			)
		)
		initContainer()

		// fixme бахнуть?
		AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

		VK.addTokenExpiredHandler(tokenTracker)

		registerActivityLifecycleCallbacks(object : DefaultActivityLifecycleCallbacks {
			override fun onActivityResumed(activity: Activity) {
				if (VK.isLoggedIn()) {
					favoritesRepo.sync()
				}
			}
		})
	}

	private fun initContainer() {
		context = this
		gson = Gson()
		prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE)
		favoritesRepo = FavoritesRepo(prefs, gson)
	}

	private val tokenTracker = object : VKTokenExpiredHandler {
		override fun onTokenExpired() {
			favoritesRepo.invalidate()

			val intent = Intent(this@App, LauncherActivity::class.java)
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
			startActivity(intent)

			Log.d("VKAuthRepository", "onTokenExpired")
		}
	}

	companion object {
		lateinit var context: Context
		lateinit var gson: Gson
		lateinit var prefs: SharedPreferences
		lateinit var favoritesRepo: FavoritesRepo
	}
}