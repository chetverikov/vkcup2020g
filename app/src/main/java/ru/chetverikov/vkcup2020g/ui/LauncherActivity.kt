package ru.chetverikov.vkcup2020g.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope
import kotlinx.android.synthetic.main.activity_launch.*
import ru.chetverikov.vkcup2020g.R

class LauncherActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		if (VK.isLoggedIn()) {
			window.setWindowAnimations(0)
			goto()
			return
		}

		setContentView(R.layout.activity_launch)
		login.setOnClickListener {
			login()
		}

		login()
	}

	// TODO репа на авторизацию?
	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		val callback = object : VKAuthCallback {
			override fun onLogin(token: VKAccessToken) {
				goto()
			}

			override fun onLoginFailed(errorCode: Int) {
				Log.d("LauncherActivity", "onLoginFailed with code $errorCode")
			}
		}
		if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
			super.onActivityResult(requestCode, resultCode, data)
		}
	}

	private fun login() {
		VK.login(this, listOf(VKScope.MARKET))
	}

	private fun goto() {
		finish()
		startActivity(Intent(this, MainActivity::class.java))
	}
}