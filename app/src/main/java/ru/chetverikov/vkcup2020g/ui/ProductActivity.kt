package ru.chetverikov.vkcup2020g.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.toolbar.*
import ru.chetverikov.vkcup2020g.App
import ru.chetverikov.vkcup2020g.R
import ru.chetverikov.vkcup2020g.model.VKProduct

class ProductActivity : AppCompatActivity() {

	private var isFavorite: Boolean = false

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_product)

		setSupportActionBar(toolbar)
		supportActionBar?.let {
			it.setDisplayHomeAsUpEnabled(true)
			it.setHomeButtonEnabled(true)
		}

		val product = InRoute.getProduct(intent)

		Glide.with(this)
			.load(product.photo)
			.into(photo)

		title = product.title
		name.text = product.title
		description.text = product.description
		price.text = product.price.text

		favorite.setOnClickListener {
			isFavorite = isFavorite.not()
			if (isFavorite) {
				App.favoritesRepo.add(product)
			} else {
				App.favoritesRepo.remove(product)
			}
			updateButtonState()
		}

		isFavorite = App.favoritesRepo.isFavorite(product)
		updateButtonState()
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			android.R.id.home -> {
				finish()
				return true
			}
		}
		return super.onOptionsItemSelected(item)
	}

	private fun updateButtonState() {
		favorite.setBackgroundResource(
			if (isFavorite) R.drawable.shape_button_inactive else R.drawable.shape_button_active
		)
		favorite.setTextColor(
			ContextCompat.getColor(
				this,
				if (isFavorite) R.color.blueAccent else R.color.whiteLabel
			)
		)
		favorite.setText(
			if (isFavorite) R.string.button_fav_remove else R.string.button_fav_add
		)
	}

	object InRoute {

		private val EXTRA_PRODUCT = "extra_product"

		fun intent(context: Context, product: VKProduct): Intent {
			val intent = Intent(context, ProductActivity::class.java)
			intent.putExtra(EXTRA_PRODUCT, product)
			return intent
		}

		fun getProduct(intent: Intent) = intent.getSerializableExtra(EXTRA_PRODUCT) as VKProduct
	}
}