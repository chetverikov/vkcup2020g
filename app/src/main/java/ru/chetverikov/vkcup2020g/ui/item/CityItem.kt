package ru.chetverikov.vkcup2020g.ui.item

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.chetverikov.vkcup2020g.R
import ru.chetverikov.vkcup2020g.model.VKCity
import ru.chetverikov.vkcup2020g.util.RVHolder
import ru.chetverikov.vkcup2020g.util.RVItem

class CityItem : RVItem<VKCity, CityItemHolder> {

	var currentCity: VKCity? = null
	var citySelectCallback: ((VKCity) -> Any)? = null

	override fun create(parent: ViewGroup): CityItemHolder {
		return CityItemHolder(parent)
	}

	override fun bind(h: CityItemHolder, entry: VKCity) {
		h.name.text = entry.title
		h.check.visibility = if (entry == currentCity) View.VISIBLE else View.GONE
		h.itemView.setOnClickListener {
			citySelectCallback?.invoke(entry)
		}
	}
}

class CityItemHolder(parent: ViewGroup) : RVHolder(R.layout.item_city, parent) {
	val name = findView<TextView>(R.id.name)
	val check = findView<View>(R.id.check)
}