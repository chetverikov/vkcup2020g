package ru.chetverikov.vkcup2020g.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_market.*
import kotlinx.android.synthetic.main.toolbar.*
import ru.chetverikov.vkcup2020g.R
import ru.chetverikov.vkcup2020g.model.VKGroup
import ru.chetverikov.vkcup2020g.model.VKItems
import ru.chetverikov.vkcup2020g.model.VKProduct
import ru.chetverikov.vkcup2020g.request.MarketGetRequest
import ru.chetverikov.vkcup2020g.ui.item.MarketGoodItem
import ru.chetverikov.vkcup2020g.util.LazyListController
import ru.chetverikov.vkcup2020g.util.RVAdapter
import ru.chetverikov.vkcup2020g.util.RequestHandler
import ru.chetverikov.vkcup2020g.util.Toaster
import kotlin.math.floor
import kotlin.math.min

class MarketActivity : AppCompatActivity() {

	private val toaster = Toaster(this)

	private val rvAdapter = RVAdapter()
	private val marketGoodItem = MarketGoodItem()

	private lateinit var productsRequest: RequestHandler<VKItems<VKProduct>>
	private lateinit var productsMoreRequest: RequestHandler<VKItems<VKProduct>>

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_market)

		val marketId = InRoute.getMarketId(intent)
		val marketName = InRoute.getMarketName(intent)

		setSupportActionBar(toolbar)
		supportActionBar?.let {
			it.setDisplayHomeAsUpEnabled(true)
			it.setHomeButtonEnabled(true)
		}
		title = "Товары сообщества $marketName"

		val spanCount = getSpanCount()
		list.layoutManager = GridLayoutManager(this, spanCount)
		list.adapter = rvAdapter

		val lazyListController = LazyListController(
			list,
			rvAdapter,
			marketGoodItem,
			min(spanCount * 5, 30),
			spanCount
		)
		lazyListController.onLoadMore = { size ->
			productsMoreRequest.execute(MarketGetRequest(marketId, size))
		}

		swipeToRefresh.setColorSchemeResources(R.color.blueAccent)
		swipeToRefresh.setOnRefreshListener {
			productsRequest.execute(MarketGetRequest(marketId))
		}

		productsRequest = RequestHandler<VKItems<VKProduct>>(lifecycle).apply {
			successCallback = { result ->
				lazyListController.reset()
				lazyListController.addItems(result.items, result.count)

				placeholder.visibility = if (result.count == 0) View.VISIBLE else View.GONE

				if (result.items.isNotEmpty()) {
					list.scrollToPosition(0)
				}

				swipeToRefresh.isRefreshing = false
			}
			failCallback = { e: Exception, _: Boolean ->
				toaster.show(R.string.network_error)
				swipeToRefresh.isRefreshing = false
			}
			execute(MarketGetRequest(marketId))
		}
		productsMoreRequest = RequestHandler<VKItems<VKProduct>>(lifecycle).apply {
			successCallback = { result ->
				lazyListController.addItems(result.items, result.count)
			}
			failCallback = { e: Exception, _: Boolean ->
				lazyListController.onFail()
			}
		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			android.R.id.home -> {
				finish()
				return true
			}
		}
		return super.onOptionsItemSelected(item)
	}

	private fun getSpanCount(): Int {
		val width = resources.displayMetrics.widthPixels
		val listPadding = resources.getDimension(R.dimen.market_goods_h_padding) * 2f
		val listSize = width - listPadding

		val goodPadding = resources.getDimension(R.dimen.market_good_h_padding) * 2f
		val goodMinSize = resources.getDimension(R.dimen.market_good_min_size)
		val fullItem = goodMinSize + goodPadding

		val num = listSize / fullItem

		marketGoodItem.photoSize = floor(listSize / num).toInt()

		return floor(num).toInt()
	}

	object InRoute {

		private val EXTRA_MARKET_ID = "extra_market_id"
		private val EXTRA_MARKET_NAME = "extra_market_name"

		fun intent(context: Context, vkGroup: VKGroup): Intent {
			val intent = Intent(context, MarketActivity::class.java)
			intent.putExtra(EXTRA_MARKET_ID, vkGroup.id)
			intent.putExtra(EXTRA_MARKET_NAME, vkGroup.name)
			return intent
		}

		fun getMarketId(intent: Intent) = intent.getIntExtra(EXTRA_MARKET_ID, -1)
		fun getMarketName(intent: Intent) = intent.getStringExtra(EXTRA_MARKET_NAME) ?: ""
	}
}