package ru.chetverikov.vkcup2020g.ui

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import ru.chetverikov.vkcup2020g.R
import ru.chetverikov.vkcup2020g.model.VKCity
import ru.chetverikov.vkcup2020g.ui.item.CityItem
import ru.chetverikov.vkcup2020g.util.RVAdapter

class CitySelectDialogFactory(
	private val context: Context
) {

	var citySelectCallback: ((VKCity) -> Any)? = null

	private val rvAdapter = RVAdapter()
	private val cityItem = CityItem()

	fun create(cities: List<VKCity>, currentCity: VKCity): Dialog {
		val dialog = BottomSheetDialog(context, R.style.Theme_VKCup_BottomSheet)

		val view = LayoutInflater.from(context).inflate(R.layout.dialog_cities, null)

		val list = view.findViewById<RecyclerView>(R.id.list)
		val close = view.findViewById<View>(R.id.close)
		close.setOnClickListener {
			dialog.dismiss()
		}

		list.layoutManager = LinearLayoutManager(context)
		list.isNestedScrollingEnabled = true
		list.adapter = rvAdapter

		cityItem.currentCity = currentCity
		cityItem.citySelectCallback = {
			citySelectCallback?.invoke(it)
			dialog.dismiss()
		}

		rvAdapter.clearItems()
		rvAdapter.addItems(cityItem, cities)
		rvAdapter.notifyDataSetChanged()

		dialog.setContentView(view)
		dialog.dismissWithAnimation = true
		dialog.setOnShowListener {
			val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(view.parent as View)
			behavior.peekHeight = view.height
		}
		return dialog
	}
}