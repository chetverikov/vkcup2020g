package ru.chetverikov.vkcup2020g.ui

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import ru.chetverikov.vkcup2020g.R
import ru.chetverikov.vkcup2020g.model.VKCity
import ru.chetverikov.vkcup2020g.model.VKGroup
import ru.chetverikov.vkcup2020g.model.VKItems
import ru.chetverikov.vkcup2020g.request.DatabaseGetCitiesRequest
import ru.chetverikov.vkcup2020g.request.GroupSearchRequest
import ru.chetverikov.vkcup2020g.ui.item.MarketHolder
import ru.chetverikov.vkcup2020g.ui.item.MarketItem
import ru.chetverikov.vkcup2020g.util.LazyListController
import ru.chetverikov.vkcup2020g.util.RVAdapter
import ru.chetverikov.vkcup2020g.util.RequestHandler
import ru.chetverikov.vkcup2020g.util.Toaster

class MainActivity : AppCompatActivity() {

	private val toaster = Toaster(this)

	private val rvAdapter = RVAdapter()
	private val marketItem = MarketItem()

	private val citySelectDialogFactory = CitySelectDialogFactory(this)
	private var currentCity: VKCity? = null
	private val cities = mutableListOf<VKCity>()

	private var dropDown: AppCompatImageView? = null

	private lateinit var lazyListController: LazyListController<VKGroup?, MarketHolder>
	private lateinit var citiesRequest: RequestHandler<VKItems<VKCity>>
	private lateinit var groupsRequest: RequestHandler<VKItems<VKGroup>>
	private lateinit var groupsMoreRequest: RequestHandler<VKItems<VKGroup>>

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setSupportActionBar(toolbar)
		setTitle(R.string.label_markets)

		list.layoutManager = LinearLayoutManager(this)
		list.adapter = rvAdapter

		lazyListController = LazyListController(list, rvAdapter, marketItem)
		lazyListController.onLoadMore = { size ->
			currentCity?.let {
				groupsMoreRequest.execute(GroupSearchRequest(it.id, size))
			}
		}

		swipeToRefresh.setColorSchemeResources(R.color.blueAccent)
		swipeToRefresh.setOnRefreshListener {
			if (currentCity == null) {
				citiesRequest.execute(DatabaseGetCitiesRequest())
			} else {
				groupsRequest.execute(GroupSearchRequest(currentCity!!.id))
			}
		}

		citySelectDialogFactory.citySelectCallback = {
			updateToolbar(it)
		}

		citiesRequest = RequestHandler<VKItems<VKCity>>(lifecycle).apply {
			successCallback = { result ->
				val city = result.items.first()
				updateToolbar(city)

				cities.clear()
				cities.addAll(result.items)

				swipeToRefresh.isRefreshing = false
			}
			failCallback = { e: Exception, withCache: Boolean ->
				cities.clear()
				swipeToRefresh.isRefreshing = false
				toaster.show(R.string.network_error)
			}
			execute(DatabaseGetCitiesRequest())
		}

		groupsRequest = RequestHandler<VKItems<VKGroup>>(lifecycle).apply {
			successCallback = { result ->
				lazyListController.reset()
				lazyListController.addItems(result.items, result.count)

				placeholder.visibility = if (result.count == 0) View.VISIBLE else View.GONE

				if (result.items.isNotEmpty()) {
					list.scrollToPosition(0)
				}

				swipeToRefresh.isRefreshing = false
			}
			failCallback = { e, withCache ->
				swipeToRefresh.isRefreshing = false
				toaster.show(R.string.network_error)
			}
		}

		groupsMoreRequest = RequestHandler<VKItems<VKGroup>>(lifecycle).apply {
			successCallback = { result ->
				lazyListController.addItems(result.items, result.count)
			}
			failCallback = { e, withCache ->
				lazyListController.onFail()
			}
		}
	}

	private fun updateToolbar(city: VKCity) {
		if (dropDown == null) {
			dropDown = AppCompatImageView(this)

			val density = resources.displayMetrics.density
			val padding: Int = (2f * density).toInt()
			val paddingTop: Int = (4f * density).toInt()

			dropDown?.apply {
				cropToPadding = true
				setPadding(padding, paddingTop, padding, padding)
				setImageResource(R.drawable.ic_expand)
				setBackgroundResource(R.drawable.shape_ripple)
				setOnClickListener {
					currentCity?.let {
						citySelectDialogFactory.create(cities, it).show()
					}
				}
			}
			val params = Toolbar.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT,
				Gravity.CENTER
			)
			toolbar.addView(dropDown, params)
		}
		currentCity = city
		title = "Магазины в ${city.title}е"
		lazyListController.reset()
		requestGroups(city.id)
	}

	private fun requestGroups(cityId: Int) {
		groupsRequest.execute(GroupSearchRequest(cityId))
	}
}