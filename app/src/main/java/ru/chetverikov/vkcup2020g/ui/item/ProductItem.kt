package ru.chetverikov.vkcup2020g.ui.item

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import ru.chetverikov.vkcup2020g.R
import ru.chetverikov.vkcup2020g.model.VKProduct
import ru.chetverikov.vkcup2020g.ui.ProductActivity
import ru.chetverikov.vkcup2020g.util.RVHolder
import ru.chetverikov.vkcup2020g.util.RVItem

class MarketGoodItem : RVItem<VKProduct?, MarketGoodHolder> {

	var photoSize: Int? = null

	override fun create(parent: ViewGroup): MarketGoodHolder = MarketGoodHolder(parent)

	override fun bind(h: MarketGoodHolder, entry: VKProduct?) {
		if (photoSize != null) {
			val params = h.photo.layoutParams
			params.width = photoSize!!
			params.height = photoSize!!
		}

		if (entry == null) {
			h.name.setBackgroundResource(R.drawable.shape_skeleton)
			h.name.text = null
			h.price.setBackgroundResource(R.drawable.shape_skeleton)
			h.price.text = null
			Glide.with(h.itemView).clear(h.photo)
			h.photo.setImageResource(R.drawable.shape_skeleton)
			h.itemView.setOnClickListener(null)
			return
		}

		h.name.background = null
		h.price.background = null

		h.name.text = entry.title
		h.price.text = entry.price.text

		Glide.with(h.context)
			.load(entry.photo)
			.override(h.photo.width)
			.apply(RequestOptions.bitmapTransform(RoundedCorners(h.dpToPx(8f).toInt())))
			.into(h.photo)

		h.itemView.setOnClickListener {
			h.context.startActivity(
				ProductActivity.InRoute.intent(h.context, entry)
			)
		}
	}
}

class MarketGoodHolder(parent: ViewGroup) : RVHolder(R.layout.item_product, parent) {
	val photo = findView<ImageView>(R.id.photo)
	val name = findView<TextView>(R.id.name)
	val price = findView<TextView>(R.id.price)
}