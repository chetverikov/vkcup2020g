package ru.chetverikov.vkcup2020g.ui.item

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.chetverikov.vkcup2020g.R
import ru.chetverikov.vkcup2020g.model.VKGroup
import ru.chetverikov.vkcup2020g.ui.MarketActivity
import ru.chetverikov.vkcup2020g.util.RVHolder
import ru.chetverikov.vkcup2020g.util.RVItem

class MarketItem : RVItem<VKGroup?, MarketHolder> {

	override fun create(parent: ViewGroup): MarketHolder =
		MarketHolder(parent)

	override fun bind(h: MarketHolder, entry: VKGroup?) {
		if (entry == null) {
			h.title.setBackgroundResource(R.drawable.shape_skeleton)
			h.title.text = null
			h.description.setBackgroundResource(R.drawable.shape_skeleton)
			h.description.text = null
			h.logo.setBackgroundResource(R.drawable.shape_skeleton)
			h.logo.setImageBitmap(null)
			Glide.with(h.itemView).clear(h.logo)
			h.itemView.setOnClickListener(null)
			return
		}

		h.title.background = null
		h.description.background = null
		h.logo.background = null

		h.title.text = entry.name
		h.description.text = entry.getTypeText()

		Glide.with(h.itemView)
			.load(entry.photo)
			.apply(RequestOptions.circleCropTransform())
			.into(h.logo)

		h.itemView.setOnClickListener {
			h.context.startActivity(
				MarketActivity.InRoute.intent(h.context, entry)
			)
		}
	}
}

class MarketHolder(parent: ViewGroup) : RVHolder(R.layout.item_market, parent) {
	val logo = findView<ImageView>(R.id.logo)
	val title = findView<TextView>(R.id.title)
	val description = findView<TextView>(R.id.description)
}