package ru.chetverikov.vkcup2020g.util

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes

class Toaster(private val context: Context) {

	fun show(@StringRes stringId: Int) {
		Toast.makeText(context, stringId, Toast.LENGTH_SHORT).show()
	}
}