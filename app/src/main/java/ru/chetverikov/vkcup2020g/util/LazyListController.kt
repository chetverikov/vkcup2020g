package ru.chetverikov.vkcup2020g.util

import androidx.recyclerview.widget.RecyclerView

@Suppress("UNCHECKED_CAST")
class LazyListController<E : Any?, H : RVHolder>(
	list: RecyclerView,
	private val rvAdapter: RVAdapter,
	private val item: RVItem<E, H>,
	private val threshold: Int = 20,
	private val placeholdersCount: Int = 5
) {

	var onLoadMore: ((size: Int) -> Unit)? = null

	private var maxCount: Int = -1
	private val entries = ArrayList<Any?>()

	private var isLoading: Boolean = true

	init {
		list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
			override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
				val isLast = isLastItem(recyclerView)
				if (isLast && !isLoading && !isEndReached()) {
					isLoading = true
					onLoadMore?.invoke(entries.size)
				}
			}
		})

		reset()
	}

	fun addItems(items: List<E>, maxCount: Int) {
		entries.addAll(items)
		this.maxCount = maxCount
		isLoading = false
		updateList()
	}

	fun onFail() {
		isLoading = false
	}

	fun reset() {
		maxCount = -1
		entries.clear()
		isLoading = true
		rvAdapter.clearItems()
		addPlaceholders()
		rvAdapter.notifyDataSetChanged()
	}

	private fun isEndReached() = entries.size >= maxCount

	private fun updateList() {
		rvAdapter.clearItems()
		if (maxCount == -1) {
			addPlaceholders()
		} else if (isEndReached()) {
			rvAdapter.addItems(item, entries as List<E>)
		} else {
			rvAdapter.addItems(item, entries as List<E>)
			val leftCount = maxCount - entries.size
			if (leftCount < placeholdersCount) {
				addPlaceholders(leftCount)
			} else {
				addPlaceholders()
			}
		}
		rvAdapter.notifyDataSetChanged()
	}

	private fun addPlaceholders(count: Int = placeholdersCount) {
		repeat(count) {
			rvAdapter.addItem(item, null as E)
		}
	}

	private fun isLastItem(list: RecyclerView): Boolean {
		if (list.childCount == 0) {
			return true
		}
		val lm = list.layoutManager
		val lastChildIndex = lm!!.childCount - 1
		val lastView = lm.getChildAt(lastChildIndex)
		return list.getChildAdapterPosition(lastView!!) >= list.adapter!!.itemCount - threshold
	}
}