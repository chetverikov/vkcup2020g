package ru.chetverikov.vkcup2020g.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.*
import androidx.collection.ArraySet
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import java.util.*

@Suppress("MemberVisibilityCanBePrivate")
class RVAdapter : RecyclerView.Adapter<ViewHolder>() {

	private val entries: MutableList<Any?> = ArrayList()
	private val binders: MutableList<RVHolderBinder<Any?, RVHolder>> = ArrayList()
	private val holders: MutableList<RVHolderFactory<RVHolder>> = ArrayList()
	private val viewTypes = ArraySet<RVHolderFactory<RVHolder>>()

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		return viewTypes.valueAt(viewType)!!.create(parent)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		binders[position].bind(holder as RVHolder, entries[position])
	}

	override fun getItemCount(): Int = entries.size

	override fun getItemViewType(position: Int): Int = viewTypes.indexOf(holders[position])

	fun clearItems() {
		entries.clear()
		binders.clear()
		holders.clear()
		viewTypes.clear()
	}

	fun <E : Any?, H : RVHolder> addItem(
		binder: RVHolderBinder<E, H>,
		holder: RVHolderFactory<H>,
		entry: E
	) {
		entries.add(entry)
		@Suppress("UNCHECKED_CAST")
		binders.add(binder as RVHolderBinder<Any?, RVHolder>)
		holders.add(holder)
		viewTypes.add(holder)
	}

	fun <E : Any?, H : RVHolder> addItems(
		binder: RVHolderBinder<E, H>,
		holder: RVHolderFactory<H>,
		entries: List<E>
	) {
		entries.forEach { addItem(binder, holder, it) }
	}

	fun <E : Any?, H : RVHolder> addItem(item: RVItem<E, H>, entry: E) {
		addItem(item, item, entry)
	}

	fun <E : Any?, H : RVHolder> addItems(item: RVItem<E, H>, entries: List<E>) {
		addItems(item, item, entries)
	}
}

interface RVItem<E : Any?, H : RVHolder> : RVHolderBinder<E, H>, RVHolderFactory<H>

interface RVHolderBinder<in E : Any?, in H : RVHolder> {
	fun bind(h: H, entry: E)
}

interface RVHolderFactory<out H : RVHolder> {
	fun create(parent: ViewGroup): H
}

open class RVHolder(@LayoutRes layout: Int, parent: ViewGroup) : ViewHolder(
	LayoutInflater.from(parent.context).inflate(layout, parent, false)
) {
	val context: Context = itemView.context
	val density: Float = context.resources.displayMetrics.density

	@Suppress("UNCHECKED_CAST")
	fun <T : View?> findView(@IdRes viewId: Int): T {
		return itemView.findViewById<View>(viewId) as T
	}

	fun dpToPx(@Dimension(unit = Dimension.DP) dp: Float) = dp * density

	fun getDrawable(@DrawableRes drawableRes: Int) = ContextCompat.getDrawable(context, drawableRes)

	@ColorInt
	fun getColor(@ColorRes colorRes: Int) = ContextCompat.getColor(context, colorRes)
}