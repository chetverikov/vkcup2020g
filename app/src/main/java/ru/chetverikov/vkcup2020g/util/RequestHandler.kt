package ru.chetverikov.vkcup2020g.util

import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import ru.chetverikov.vkcup2020g.BuildConfig
import ru.chetverikov.vkcup2020g.request.BaseRequest

class RequestHandler<E>(
	private val lifecycle: Lifecycle
) : DefaultLifecycleObserver {

	var successCallback: ((result: E) -> Unit)? = null
	var failCallback: ((e: Exception, withCache: Boolean) -> Unit)? = null
	var loadingCallback: ((withCache: Boolean) -> Unit)? = null

	private var cache: E? = null

	init {
		lifecycle.addObserver(this)
	}

	override fun onResume(owner: LifecycleOwner) {
		cache?.let {
			log("Resuming with cache $it")
			successCallback?.invoke(it)
		}
	}

	fun execute(request: BaseRequest<E>) {
		log("Executing request ${request.javaClass.simpleName} with params ${request.params}")
		loadingCallback?.invoke(isCached())
		VK.execute(request, object : VKApiCallback<E> {
			override fun fail(error: Exception) {
				log("Fail with error: $error")
				if (isResumed()) {
					failCallback?.invoke(error, isCached())
				}
			}

			override fun success(result: E) {
				log("Success with result: $result")
				if (isResumed()) {
					successCallback?.invoke(result)
				} else {
					cache = result
				}
			}
		})
	}

	fun isCached() = cache != null

	private fun isResumed() = lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)

	private fun log(message: String) {
		if (BuildConfig.DEBUG) {
			Log.d("RequestHandler", message)
		}
	}
}