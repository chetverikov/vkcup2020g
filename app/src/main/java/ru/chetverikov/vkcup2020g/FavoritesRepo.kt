package ru.chetverikov.vkcup2020g

import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import ru.chetverikov.vkcup2020g.model.VKFavoriteProduct
import ru.chetverikov.vkcup2020g.model.VKItems
import ru.chetverikov.vkcup2020g.model.VKProduct
import ru.chetverikov.vkcup2020g.request.FaveAddProductRequest
import ru.chetverikov.vkcup2020g.request.FaveGetRequest
import ru.chetverikov.vkcup2020g.request.FaveRemoveProductRequest
import java.util.concurrent.TimeUnit

private const val TAG = "FavoritesRepo"
private const val PREFS_CACHE = "fav_cache"
private const val PREFS_CACHE_TIMESTAMP = "fav_cache_timestamp"
private const val NULL_SYNC_TIMESTAMP = 0L
private const val CACHE_ACTUAL_MINUTES = 1L

class FavoritesRepo(
	private val sharedPreferences: SharedPreferences,
	private val gson: Gson
) {

	// TODO сделать тупо Set?
	private val cache = LinkedHashMap<Int, VKProduct>()
	private var lastSyncTimestamp: Long = NULL_SYNC_TIMESTAMP

	init {
		val diskCache: String? = sharedPreferences.getString(PREFS_CACHE, null)
		if (diskCache != null) {
			val diskMap: LinkedHashMap<Int, VKProduct> = gson.fromJson(
				diskCache,
				object : TypeToken<LinkedHashMap<Int, VKProduct>>() {}.type
			)
			cache.clear()
			cache.putAll(diskMap)
		}

		val diskCacheTimestamp: Long? = sharedPreferences.getLong(PREFS_CACHE_TIMESTAMP, 0L)
		if (diskCacheTimestamp != null && diskCacheTimestamp < System.currentTimeMillis()) {
			lastSyncTimestamp = diskCacheTimestamp
		}
	}

	fun sync() {
		if (isCacheActual()) {
			return
		}
		log("sync")
		VK.execute(FaveGetRequest(), object : VKApiCallback<VKItems<VKFavoriteProduct>> {
			override fun fail(error: Exception) {
				Log.e(TAG, "sync, fail", error)
			}

			override fun success(result: VKItems<VKFavoriteProduct>) {
				log("sync, result count = ${result.count}")
				lastSyncTimestamp = System.currentTimeMillis()
				cache.clear()
				for (favorite in result.items) {
					val product = favorite.product
					cache[product.id] = product
				}
				saveCache()
			}
		})
	}

	fun isFavorite(product: VKProduct): Boolean {
		return cache[product.id] != null
	}

	fun add(product: VKProduct) {
		log("add - $product")
		VK.execute(FaveAddProductRequest(product.ownerId, product.id), object : VKApiCallback<Int> {
			override fun fail(error: Exception) {
				Log.e(TAG, "add, fail", error)
			}

			override fun success(result: Int) {
				log("add, result = $result")
				cache[product.id] = product
				saveCache()
			}
		})
	}

	fun remove(product: VKProduct) {
		log("remove - $product")
		VK.execute(FaveRemoveProductRequest(product.ownerId, product.id), object : VKApiCallback<Int> {
			override fun fail(error: Exception) {
				Log.e(TAG, "remove, fail", error)
			}

			override fun success(result: Int) {
				log("remove, result = $result")
				cache.remove(product.id)
				saveCache()
			}
		})
	}

	fun invalidate() {
		cache.clear()
		lastSyncTimestamp = NULL_SYNC_TIMESTAMP
		saveCache()
	}

	private fun saveCache() {
		sharedPreferences.edit()
			.putString(PREFS_CACHE, gson.toJson(cache))
			.putLong(PREFS_CACHE_TIMESTAMP, lastSyncTimestamp)
			.apply()
	}

	private fun isCacheActual(): Boolean {
		return System.currentTimeMillis() - lastSyncTimestamp < TimeUnit.MINUTES.toMillis(CACHE_ACTUAL_MINUTES)
	}

	private fun log(message: String) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, message)
		}
	}
}