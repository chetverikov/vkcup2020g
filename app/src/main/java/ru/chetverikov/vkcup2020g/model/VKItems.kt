package ru.chetverikov.vkcup2020g.model

import androidx.annotation.Keep

@Keep
data class VKItems<T>(
	val count: Int,
	val items: List<T>
)