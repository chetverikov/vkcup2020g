package ru.chetverikov.vkcup2020g.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class VKProduct(
	val id: Int,
	@SerializedName("owner_id")
	val ownerId: Int,
	val title: String,
	val description: String,
	val price: VKMarketPrice,
	@SerializedName("thumb_photo")
	val photo: String
) : Serializable