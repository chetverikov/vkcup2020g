package ru.chetverikov.vkcup2020g.model

import androidx.annotation.Keep
import java.io.Serializable

@Keep
data class VKMarketPrice(val text: String) : Serializable