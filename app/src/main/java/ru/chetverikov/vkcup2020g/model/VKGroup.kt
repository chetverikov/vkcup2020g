package ru.chetverikov.vkcup2020g.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class VKGroup(
	val id: Int,
	val name: String,
	val type: String,
	@SerializedName("is_closed")
	val privacyStatus: Int,
	@SerializedName("photo_200")
	val photo: String
) {

	fun getTypeText(): String {
		val status = if (type == "group" || type == "page") {
			when (privacyStatus) {
				0 -> "Открытая "
				1 -> "Закрытая "
				2 -> "Частная "
				else -> null
			}
		} else {
			when (privacyStatus) {
				0 -> "Открытое "
				1 -> "Закрытое "
				2 -> "Частное "
				else -> null
			}
		}
		var typeText = when (type) {
			"group" -> "группа"
			"page" -> "публичная страница"
			"event" -> "мероприятие"
			else -> ""
		}
		if (status != null) {
			typeText = status + typeText
		}
		return typeText
	}
}