package ru.chetverikov.vkcup2020g.model

import androidx.annotation.Keep

@Keep
data class VKCity(val id: Int, val title: String)