package ru.chetverikov.vkcup2020g.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class VKFavoriteProduct(
	@SerializedName("added_date")
	val addedDate: Long,
	val product: VKProduct
)