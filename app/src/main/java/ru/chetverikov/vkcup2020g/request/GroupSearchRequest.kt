package ru.chetverikov.vkcup2020g.request

import com.google.gson.reflect.TypeToken
import ru.chetverikov.vkcup2020g.model.VKGroup
import ru.chetverikov.vkcup2020g.model.VKItems

class GroupSearchRequest(cityId: Int, offset: Int = 0) : BaseRequest<VKItems<VKGroup>>("groups.search") {

	init {
		addParam("q", " ")
		addParam("market", 1)
		addParam("city_id", cityId)
		addParam("offset", offset)
		addParam("count", 50)
	}

	override fun getType() = object : TypeToken<VKItems<VKGroup>>() {}
}