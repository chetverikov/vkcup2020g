package ru.chetverikov.vkcup2020g.request

import com.google.gson.reflect.TypeToken
import ru.chetverikov.vkcup2020g.model.VKItems
import ru.chetverikov.vkcup2020g.model.VKProduct

class MarketGetRequest(ownerId: Int, offset: Int = 0) : BaseRequest<VKItems<VKProduct>>("market.get") {

	init {
		addParam("owner_id", -ownerId)
		addParam("extended", 1) // todo nada?
		addParam("offset", offset)
		addParam("count", 50)
	}

	override fun getType() = object : TypeToken<VKItems<VKProduct>>() {}
}