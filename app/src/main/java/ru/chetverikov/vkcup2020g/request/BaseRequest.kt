package ru.chetverikov.vkcup2020g.request

import com.google.gson.reflect.TypeToken
import com.vk.api.sdk.VKApiManager
import com.vk.api.sdk.VKMethodCall
import com.vk.api.sdk.requests.VKRequest
import org.json.JSONObject
import ru.chetverikov.vkcup2020g.App

abstract class BaseRequest<T>(method: String) : VKRequest<T>(method) {

	override fun onExecute(manager: VKApiManager): T {
		val config = manager.config

		params["lang"] = config.lang
		params["device_id"] = config.deviceId.value

		val version: String = if (params["v"] == null) config.version else params["v"].toString()
		params["v"] = version

		return manager.execute(
			VKMethodCall.Builder()
				.args(params)
				.method(method)
				.version(version)
				.build(), this
		)
	}

	override fun parse(r: JSONObject): T {
		val response = r.get("response")
		return App.gson.fromJson(response.toString(), getType().type)
	}

	abstract fun getType(): TypeToken<T>
}