package ru.chetverikov.vkcup2020g.request

import com.google.gson.reflect.TypeToken
import com.vk.api.sdk.VKApiManager
import ru.chetverikov.vkcup2020g.model.VKFavoriteProduct
import ru.chetverikov.vkcup2020g.model.VKItems

class FaveGetRequest : BaseRequest<VKItems<VKFavoriteProduct>>("fave.get") {

	init {
		addParam("item_type", "product")
		addParam("offset", 0)
		addParam("count", COUNT)
		addParam("v", "5.103")
	}

	override fun onExecute(manager: VKApiManager): VKItems<VKFavoriteProduct> {
		val items = super.onExecute(manager)
		if (items.count <= COUNT) {
			return items
		}
		val fullItemsList = ArrayList<VKFavoriteProduct>()
		val fullItems = VKItems(items.count, fullItemsList)
		val pages: Int = items.count / COUNT
		for (page in 1..pages) {
			addParam("offset", page * COUNT)
			fullItemsList.addAll(super.onExecute(manager).items)
		}
		return fullItems
	}

	override fun getType() = object : TypeToken<VKItems<VKFavoriteProduct>>() {}

	companion object {
		const val COUNT = 20
	}
}