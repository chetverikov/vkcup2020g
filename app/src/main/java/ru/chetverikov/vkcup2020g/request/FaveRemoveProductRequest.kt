package ru.chetverikov.vkcup2020g.request

import com.google.gson.reflect.TypeToken

class FaveRemoveProductRequest(ownerId: Int, id: Int) : BaseRequest<Int>("fave.removeProduct") {

	init {
		addParam("owner_id", ownerId)
		addParam("id", id)
		addParam("v", "5.103")
	}

	override fun getType() = object : TypeToken<Int>() {}
}