package ru.chetverikov.vkcup2020g.request

import com.google.gson.reflect.TypeToken
import ru.chetverikov.vkcup2020g.model.VKCity
import ru.chetverikov.vkcup2020g.model.VKItems

class DatabaseGetCitiesRequest : BaseRequest<VKItems<VKCity>>("database.getCities") {

	init {
		addParam("country_id", 1)
		addParam("need_all", 0)
	}

	override fun getType() = object : TypeToken<VKItems<VKCity>>() {}
}