
#Keep annotations
-keep @androidx.annotation.Keep class *
-keepclassmembers @androidx.annotation.Keep class * {
    <fields>;
}

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}